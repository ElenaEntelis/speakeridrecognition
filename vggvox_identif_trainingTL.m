%% Experiment with the cnn_mnist_fc_bnorm

[net_bn, info_bn] = cnn_mnist(...
  'expDir','Vox2NNcheckpoints-bnorm' , 'batchNormalization', true);%'data/mnist-bnorm'
save('Vox2NNmodel/vggvox_ident_netMyLearned.mat','net_bn');

[net_fc, info_fc] = cnn_mnist(...
  'expDir','Vox2NNcheckpoints-baseline' , 'batchNormalization', false);%'data/mnist-baseline'

function [net, info] = cnn_mnist(varargin)
%CNN_MNIST  Demonstrates MatConvNet on MNIST

%run(fullfile(fileparts(mfilename('fullpath')),...
%  '..', '..', 'matlab', 'vl_setupnn.m')) ;

opts.modelPath = '' ;
opts.batchNormalization = false ;
opts.network = [] ;
opts.networkType = 'simplenn' ;
[opts, varargin] = vl_argparse(opts, varargin) ;

sfx = opts.networkType ;
if opts.batchNormalization, sfx = [sfx '-bnorm'] ; end
opts.expDir = fullfile(vl_rootnn, 'data', ['mnist-baseline-' sfx]) ;%interim network states
[opts, varargin] = vl_argparse(opts, varargin) ;
opts.gpu = 0;%3;
opts.dataDir = fullfile(vl_rootnn, 'data', 'mnist') ;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');%data
opts.train = struct() ;
opts = vl_argparse(opts, varargin) ;
if ~isfield(opts.train, 'gpus'), opts.train.gpus = []; end;
opts.mode  ='train'; 
opts.batchSize = 64 ;
opts.numEpochs =1;% 30 ;
opts.learningRate = 0.01 ;
opts.weightDecay = 0.0005 ;
opts.errorFunction='multiclass';%'binary'
% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

% if isempty(opts.network)
%   net = cnn_mnist_init('batchNormalization', opts.batchNormalization, ...
%     'networkType', opts.networkType) ;
% else
%   net = opts.network ;
%   opts.network = [] ;
% end
% 
% if exist(opts.imdbPath, 'file')
%   imdb = load(opts.imdbPath) ;
% else
%   imdb = getMnistImdb(opts) ;
%   mkdir(opts.expDir) ;
%   save(opts.imdbPath, '-struct', 'imdb') ;
% end
% 
% net.meta.classes.name = arrayfun(@(x)sprintf('%d',x),1:10,'UniformOutput',false) ;


datafolder = 'Vox2_Data';
ads = audioDatastore(datafolder, ...
    'IncludeSubfolders',true, ...
    'FileExtensions','.m4a', ...
    'LabelSource','foldernames');
ads0 = copy(ads);


%TODO: TEST WETHER ADSTs EXIST ALREADY IN CORRESPONDENT DIRECTORY, AND IF
%YES, SPLIT ONLY REST PART OF FULL ADS INTO adsTr and adsVal, while using
%test set files as mask to exclude these files from splitting.
if exist('adsTs.mat', 'file')
	load('adsTs','Files','Labels');
    [adsTr,adsVal] = splitEachLabel(subset(ads0,~ismember(ads0.Files,Files)),0.82);%0.7,0.15
else
	[adsTr,adsVal,adsTs] = splitEachLabel(ads0,0.7,0.15);
    Files = adsTs.Files;
    Labels = adsTs.Labels;
	save('adsTs.mat','Files','Labels');
end


%-------------------------------------------------------------------------
%                                           Prepare network
%-------------------------------------------------------------------------
% Load or download the VGGVox model for Identification

if exist('Vox2NNmodel/vggvox_ident_netMy.mat', 'file')
    opts.modelPath = 'Vox2NNmodel/vggvox_ident_netMy.mat';
    tmp = load(opts.modelPath); net = tmp.net ;
else

    modelName = 'vggvox_ident_net.mat' ;
    paths = {opts.modelPath, ...
        modelName, ...
        fullfile(vl_rootnn, 'data', 'models-import', modelName)} ; %'C:\MatConvNet\matconvnet-1.0-beta25\data\models-import\vggvox_ident_net.mat'
    ok = find(cellfun(@(x) exist(x, 'file'), paths), 1) ;

    if isempty(ok)
        fprintf('Downloading the VGGVox model for Identification ... this may take a while\n') ;
        opts.modelPath = fullfile(vl_rootnn, 'data/models-import', modelName) ;
        mkdir(fileparts(opts.modelPath)) ; base = 'http://www.robots.ox.ac.uk' ;
        url = sprintf('%s/~vgg/data/voxceleb/models/%s', base, modelName) ;
        urlwrite(url, opts.modelPath) ; % writes model from internet address url to computer path opts.modelPath
    else
        opts.modelPath = paths{ok} ;
    end

    tmp = load(opts.modelPath); net = tmp.net ;
    vl_simplenn_display(net);
    % RECONSTRUCT FC LAYER OF PRELEARNED MODEL FROM ARTICLE VGGVOX TO
    %BE TWO NEW CLASSES' LAYER instead of 1251 classes' layer:
     net.layers{19}.weights =  [{0.05*randn(1,1,1024,2, 'single'), zeros(2, 1, 'single')}]; % 2 - class number: 1) id0012; 2) id0015_0016_0018
     net.layers{20}.type = 'softmaxloss';
     net.meta.classes.name  = categorical(["id00015";"id12_16_18"']);
     net.meta.classes.description   =  {"id00015";"id12_16_18"};
     net.meta.normalization.border = [0 0];
     % optionally switch to batch normalization
    if opts.batchNormalization
         net = insertBnorm(net, 1) ;
         net = insertBnorm(net,5) ;
         net = insertBnorm(net,9) ;
         net = insertBnorm(net, 12) ;
         net = insertBnorm(net, 15) ;
         net = insertBnorm(net,19) ;
          net = insertBnorm(net, 23) ;
         net = insertBnorm(net, 26) ;
    end
     vl_simplenn_display(net);
     save('Vox2NNmodel/vggvox_ident_netMy.mat','net');
end
% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

net.meta.trainOpts.numEpochs =30;
[net, info] = cnn_trainMy(net, adsTr,adsVal,getBatch, ... 
  'expDir', opts.expDir, ...
  net.meta.trainOpts) ;%, ...
                        %   'val', find(imdb.images.set == 3)

end
% --------------------------------------------------------------------
function fn = getBatch() %opts
% --------------------------------------------------------------------
%   case 'simplenn'
    fn = @(x,y,z) getSimpleNNBatch(x,y,z) ;
end

% --------------------------------------------------------------------
function [spectrograms, labels] = getSimpleNNBatch(ads,net, batch)
% --------------------------------------------------------------------
% images = imdb.images.data(:,:,:,batch) ;
% labels = imdb.images.labels(1,batch) ;
ads_batch = subset(ads,batch);
audioCellarray = readall(ads_batch); % z = audioread(audfile);
audioFilesarray = ads_batch.Files;
spectrograms = zeros(512,300,1,length(batch),'single');
 Labels = ads_batch.Labels;
 labels = zeros(numel(Labels),1,'double');
 for i = 1:numel(Labels)
    labels(i) =  find((net.meta.classes.name==Labels(i) )==1);
 end
   rng(); 
   for ib = 1:length(batch)
      
       % random excerption of 3 sec (48000) samples from ib-th audio file:
      ind = floor(rand*length(audioCellarray{ib}));
      if ind <  length(audioCellarray{ib}) - 48240 %16000Hz *3sec=48000 samples,+240 - to get 300 windows
           startind = ind;
      else
          startind = length(audioCellarray{ib}) - 48240;
      end
        SPEC 		= runSpec(audioCellarray{ib}(startind:startind + 48240 - 1),net.meta.audio);
         mu    		= mean(SPEC,2);
         stdev 		= std(SPEC,[],2) ;
        nSPEC 		= bsxfun(@minus, SPEC, mu);
        nSPEC 		= bsxfun(@rdivide, nSPEC, stdev);
      spectrograms(:,:,1,ib) = nSPEC;
       
   end
 end
% --------------------------------------------------------------------
function net = insertBnorm(net, l)
% --------------------------------------------------------------------
assert(isfield(net.layers{l}, 'weights'));
ndim = size(net.layers{l}.weights{1}, 4);
layer = struct('type', 'bnorm', ...
               'weights', {{ones(ndim, 1, 'single'), zeros(ndim, 1, 'single')}}, ...
               'learningRate', [1 1 0.05], ...
               'weightDecay', [0 0]) ;
net.layers{l}.weights{2} = [] ;  % eliminate bias in previous conv layer
net.layers = horzcat(net.layers(1:l), layer, net.layers(l+1:end)) ;
end