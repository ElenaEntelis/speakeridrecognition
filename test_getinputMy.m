function inp = test_getinputMy(image,meta) %,buckets

	audfile 	= image;% [image(1:end-3),'wav'];
	z         	= audioread(audfile);
    
  
    % random excerption of 3 sec (48000) samples from z audio file:
      ind = floor(rand*length(z));
      if ind <  length(z) - 48240 %16000Hz *3sec=48000 samples,+240 - to get 300 windows
           startind = ind;
      else
          startind = length(z) - 48240;
      end
      player = audioplayer(z,16000);
      playblocking(player,[startind,startind + 48240 - 1]);
      
      SPEC 		= runSpec(z(startind:startind + 48240 - 1),meta.audio);
    
	mu    		= mean(SPEC,2);
    stdev 		= std(SPEC,[],2) ;
    nSPEC 		= bsxfun(@minus, SPEC, mu);
    nSPEC 		= bsxfun(@rdivide, nSPEC, stdev);

%     rsize 	= buckets.width(find(buckets.width(:)<=size(nSPEC,2),1,'last'));
%     rstart  = round((size(nSPEC,2)-rsize)/2);

	inp(:,:) = single(nSPEC);%gpuArray(single(nSPEC(:,rstart:rstart+rsize-1)));

end 

