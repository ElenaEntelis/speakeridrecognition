function demo_vggvox_identif(varargin)
% DEMO_VGGVOX_IDENTIF - minimal demo with the VGGVox model pretrained on
% the VoxCeleb dataset for Speaker Identification

vl_setupnn ;

% downloads the VGGVox model and
% prints the class and score of a test speech segment


opts.modelPath = '' ;
opts.gpu = 0;%3;
opts.dataDir = 'testfiles/ident';
opts = vl_argparse(opts, varargin) ;

% Example speech segments for inputaddpath 'C:\SpeakerIDrecogn\VGGVox-master\mfcc'
% inpPath = fullfile(opts.dataDir, 'Y8hIVOBuels_0000002.wav');  %This speech segment belongs to speaker class 1

% Load or download the VGGVox model for Identification
if ~exist('Vox2NNmodel\vggvox_ident_netMyLearnedDeployed.mat','file')
    if exist('Vox2NNmodel\vggvox_ident_netMyLearned.mat','file')
            opts.modelPath ="C:\SpeakerIDrecogn\VGGVox-master\Vox2NNmodel\vggvox_ident_netMyLearned.mat";
            tmp = load(opts.modelPath); net = tmp.net_bn ;
             net = cnn_imagenet_deploy(net);
    else
        disp('there is not learned model!')
        pause(7);
        exit(0);
    end
else
    opts.modelPath ="C:\SpeakerIDrecogn\VGGVox-master\Vox2NNmodel\vggvox_ident_netMyLearnedDeployed.mat";
     tmp = load(opts.modelPath); net = tmp.net_bn ;
end
net.meta.classes.description{1} = "Aaron_Motsoaledi ";  
net.meta.classes.description{2} = "Aaron_Ashmore_Ramsey_Schock ";
% Evaluate network either on CPU or GPU and set up network to be in test
% mode
if ~isempty(opts.gpu),net = vl_simplenn_move(net,'cpu'); end
net.mode = 'test';
net.conserveMemory = false;




if exist('adsTs.mat', 'file')
	load('adsTs','Files','Labels');
else
    disp('There are not test files');
    pause(7);
    exit(0);
end

accuracy = 0;
step = 20;%40;
for i=1:step:length(Files)

    inpPath = Files{i};
    inp = test_getinputMy(inpPath,net.meta);
    net.layers{end}.class =find(net.meta.classes.name==Labels(i)) ;
    res = vl_simplenn(net,inp);
    prob 		= gather(squeeze(res(end).x(:,:,:,:)));
    prob 		= sum(prob,2);
    [score,class]    = max(prob);
    if class==net.layers{end}.class
        accuracy = accuracy +1;
    end
    ClassNName = sprintf("Person: %s	class  %d	True Person:    %s	True class %d",net.meta.classes.description{class},class,net.meta.classes.description{Labels(i)}, Labels(i));

    NET.addAssembly('System.Speech');
    obj = System.Speech.Synthesis.SpeechSynthesizer;
    obj.Volume = 100;
    Speak(obj, ClassNName);

    filename = inpPath(max(strfind(inpPath,'\'))+1:end);
    % Print score and class for the speech segment
    fprintf('i:	%4d file: %30s Person:    %30s Class:%d	True Person:    %30s True class:%d Score:%2.0f%%\n',i,filename, net.meta.classes.description{class},class,net.meta.classes.description{Labels(i)},Labels(i),score*100);
    pause(5);
end
accuracy = accuracy/(floor(length(Files)/step)+1)*100;
fprintf("accuracy: %2.0f%%",accuracy);
accuracyVocal = sprintf("accuracy: %2.0f%%",accuracy);
Speak(obj, accuracyVocal);